/*
* Users
* Angelito
*/
$(function() {
	$('#data_table_users').DataTable();
	$('#remove').remove();
});

$("#insertButton").click(function(){
	if(validateEmail($('#email').val())) {
		var data = $('#insert-form').serialize();
			$.ajax({
				type: 'POST',
				url: '/barangay_poblacion/users/insertUser',
				data: data,
				cache: false,
				success: function(data)
				{
					alert("Successfully added.");
					window.location.href = "Users";
				}
			});
	}else{
		alert("Invalid Email Address");
	}
});

$("#updateButton").click(function(){
	if(validateEmail($('#email').val())) {
		var data = $('#update-form').serialize();
			$.ajax({
				type: 'POST',
				url: '/barangay_poblacion/users/update_user_by_id',
				data: data,
				cache: false,
				success: function(data)
				{
					console.log(data);
					if ( data == 'success') {
						alert("Successfully updated.");
						window.location.href = "display";
					}
					else
						alert("An error occured");
				}
			});
	} else{
		alert("Invalid Email Address");
	}
});

$('.delete-user').click( function() {
	delete_user($(this).prop('id'));
});

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isLife(isDead){
	if(isDead == "dead") {
		document.getElementById('dead').style.display = "block";
	}else if(isDead == "alive"){
		document.getElementById('dead').style.display = "none";
	}
}

function delete_user( id ) {
	var ans = confirm('Are you sure you want to delete this record?');
	if (ans == true) {
		$.ajax({
			type: 'POST',
			url: '/barangay_poblacion/users/delete_user',
			data: { 'id': id },
			cache: false,
			success: function(data)
			{
				if ( data == 'success') {
					alert("Record successfully deleted.");
					window.location.href = "display";
				}
				else
					alert("An error occured");
			}
		});
	} else {

	}
}
/*
* END Users
* Angelito
*/

/*
* Transactions
* Roselyn
*/
$(function() {
  if ( getUrlParameter('login') == 'error' ) {
    Materialize.toast('Invalid login. Please try again.', 4000)
  } else if ( getUrlParameter('login') == 'success' ) {
    window.location.href = base_url;
  }

  $('#data_table_certificate').DataTable();
  $('#data_table_clearance').DataTable();
});

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
/*
* END Transactions
* Roselyn
*/
