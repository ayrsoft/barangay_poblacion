  angular.module('app').controller('TransactionsCtrl', ['$scope', '$http', '$mdDialog', '$rootScope', '$timeout', function($scope, $http, $mdDialog, $rootScope, $timeout){

    // query list of document types
    $http.get(base_url + 'transactions/get_doc_types')
        .then(function(res) {
          $scope.documents = res.data;
        });

    // query list of residents
    $http.get(base_url + 'transactions/get_residents')
        .then(function(res) {
          $scope.residents = res.data;
        });

    $http.get(base_url + 'transactions/get_street_names')
        .then(function(res) {
          $scope.street_names = res.data;
        });

    var tabs = [
          ],
          selected = null,
          previous = null;
    $scope.tabs = tabs;

    $scope.addTab = function (title) {
      tabs.push({ document: title, disabled: false});
    };

    $scope.removeTab = function (tab) {
      var index = tabs.indexOf(tab);
      tabs.splice(index, 1);
      delete transact.forms['tab' + index];
    };

    var transact = this;

    $scope.submitForms = function () {
      console.log(transact.forms);
      $http.post(base_url + 'transactions/save_transactions', transact.forms)
          .then(function(res) {
            console.log(res.data);
            if ( res.data['status'] == "success" ) {
              tabs = [
                    ],
                    selected = null,
                    previous = null;
              $scope.tabs = tabs;
              transact.forms = {};
              $scope.links = res.data['ids'];

              htmlContent = "";
              for (var i = 0; i < res.data['ids'].length; i++) {
                htmlContent += "<div><a href='" + base_url + "pdfs/" + res.data['ids'][i] + ".pdf'>Form " + (i + 1) + "</a></div>";
              }

              $mdDialog.show(
                $mdDialog.alert()
                  .clickOutsideToClose(true)
                  .title('PDF Links')
                  .htmlContent(htmlContent)
                  .ok('Close')
              );
            }
          });
    }

    $scope.init_datepicker = function(tab_name) {
      $timeout(function(){
        $('#datepicker_' + tab_name).datepicker({
          minDate: 0,
          dateFormat:'MM d, yy',
          onSelect: function(dateText) {
            $scope.selecteddate = dateText;
            $(this).val(dateText);
            $(this).change();
          }
        });

        $('#date1_' + tab_name).datepicker({
          minDate: 0,
          dateFormat:'MM d, yy',
          onSelect: function(dateText) {
            $scope.selecteddate = dateText;
            $(this).val(dateText);
            $(this).change();
          }
        });

        $('#date2_' + tab_name).datepicker({
          minDate: 0,
          dateFormat:'MM d, yy',
          onSelect: function(dateText) {
            $scope.selecteddate = dateText;
            $(this).val(dateText);
            $(this).change();
          }
        });
      });
    }

}]);
