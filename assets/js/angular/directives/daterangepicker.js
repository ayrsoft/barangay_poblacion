angular.module('app').directive('daterangepicker', function() {
  return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
          console.log(element);
          element.daterangepicker(
          {
            minDate: moment(),
            locale: {
              format: 'YYYY-MM-DD'
            }
          }
        }
    };
});
