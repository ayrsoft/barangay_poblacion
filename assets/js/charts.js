
$(function() {
	console.log("chart loaded");

	var transactionsData = [];
	var financeChart, transactionsChart;

	var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "light",
    "marginRight":30,
    "legend": {
        "equalWidths": false,
        "periodValueText": "total: [[value.sum]]",
        "position": "top",
        "valueAlign": "left",
        "valueWidth": 100
    },
    "dataProvider": [{
				"date": "January 2017",
        "cars": 1587,
        "motorcycles": 650,
        "bicycles": 121
    }, {
				"date": "May 2017",
        "cars": 1567,
        "motorcycles": 683,
        "bicycles": 146
    }],
    "valueAxes": [{
        "stackType": "regular",
        "gridAlpha": 0.07,
        "position": "left",
        "title": "Traffic incidents"
    }],
    "graphs": [{
        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/car.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "hidden": true,
        "lineAlpha": 0.4,
        "title": "Cars",
        "valueField": "cars"
    }, {
        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/motorcycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "lineAlpha": 0.4,
        "title": "Motorcycles",
        "valueField": "motorcycles"
    }, {
        "balloonText": "<img src='http://www.amcharts.com/lib/3/images/bicycle.png' style='vertical-align:bottom; margin-right: 10px; width:28px; height:21px;'><span style='font-size:14px; color:#000000;'><b>[[value]]</b></span>",
        "fillAlphas": 0.6,
        "lineAlpha": 0.4,
        "title": "Bicycles",
        "valueField": "bicycles"
    }],
    "plotAreaBorderAlpha": 0,
    "marginTop": 10,
    "marginLeft": 0,
    "marginBottom": 0,
    "chartScrollbar": {},
    "chartCursor": {
        "cursorAlpha": 0
    },
    "categoryField": "date",
    "categoryAxis": {
        "startOnAxis": true,
        "axisColor": "#DADADA",
        "gridAlpha": 0.07,
        "title": "Year",
        "guides": [{
            category: "2001",
            toCategory: "2003",
            lineColor: "#CC0000",
            lineAlpha: 1,
            fillAlpha: 0.2,
            fillColor: "#CC0000",
            dashLength: 2,
            inside: true,
            labelRotation: 90,
            label: "fines for speeding increased"
        }, {
            category: "2007",
            lineColor: "#CC0000",
            lineAlpha: 1,
            dashLength: 2,
            inside: true,
            labelRotation: 90,
            label: "motorcycle fee introduced"
        }]
    },
    "export": {
    	"enabled": true
     }
});

	/*
		Year - YYYY
		Month - MM
		Day - DD
		Clearances/Certificates - integer
	*/

	$.ajax({
		type: "get",
		url: "Auth/getTransactionsData",
		dataType: "json",
		success: function(data){
			transactionsChart = AmCharts.makeChart("transactionsChart", {
			    "type": "serial",
			    "theme": "light",
			    "dataDateFormat": "YYYY-MM-DD",
			    "marginRight":15,
			    "mouseWheelZoomEnabled": true,
			    "legend": [{
			        "equalWidths": false,
			        "periodValueText": "total: [[value.sum]]",
			        "position": "top",
			        "valueAlign": "left",
			        "valueWidth": 100
			    }],
			    "valueAxes": [{
			        "stackType": "regular",
			        "gridAlpha": 0.07,
			        "position": "left",
			        "title": "Transactions",
			        "precision": 0
			    }],
			    "dataProvider": data,
			    "graphs": [{
			        "id": "clearanceGraph",
			        "balloon":{
			          "adjustBorderColor":false,
			          "color":"#ffffff"
			        },
			        "bullet": "round",
			        "bulletBorderAlpha": 1,
			        "bulletColor": "#FFFFFF",
			        "bulletSize": 5,
			        "hideBulletsCount": 50,
			        "lineThickness": 2,
			        "fillAlphas": 0.5,
			        "title": "Clearances",
			        "useLineColorForBulletBorder": true,
			        "valueField": "clearances",
			        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
			    }, {
			        "id": "certificateGraph",
			        "balloon":{
			          "adjustBorderColor":false,
			          "color":"#ffffff"
			        },
			        "bullet": "round",
			        "bulletBorderAlpha": 1,
			        "bulletColor": "#000000",
			        "bulletSize": 5,
			        "hideBulletsCount": 50,
			        "lineThickness": 2,
			        "fillAlphas": 0.5,
			        "title": "Certificates",
			        "useLineColorForBulletBorder": true,
			        "valueField": "certificates",
			        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
			    }],
			    "plotAreaBorderAlpha": 0,
			    "marginTop": 10,
			    "marginLeft": 0,
			    "marginBottom": 0,
			    "chartScrollbar": {},
			    "chartCursor": {
			        "cursorAlpha": 0
			    },
			    "categoryField": "date",
			    "categoryAxis": {
					"startOnAxis": true,
					"axisColor": "#DADADA",
					"gridAlpha": 0.07,
					"title": "Date"
			    },
			    "export": {
			    	"enabled": true
			    }
			});
		},
		error: function(xhr){
			alert("Error: Unable to fetch transactions data");
		}
	});

/*
	FINANCE CHART
*/

	$.ajax({
		type: "get",
		url: "Auth/getFinanceData",
		dataType: "json",
		success: function(data){
			financeChart = AmCharts.makeChart("financeChart", {
			    "type": "serial",
			    "theme": "light",
			    "marginRight": 15,
			    "marginLeft": 40,
			    "autoMarginOffset": 20,
			    "mouseWheelZoomEnabled":true,
			    "dataDateFormat": "YYYY-MM-DD",
			    "valueAxes": [{
			        "id": "v1",
			        "axisAlpha": 0,
			        "position": "left",
			        "ignoreAxisWidth":true,
			        "title": "Profit"
			    }],
			    "balloon": {
			        "borderThickness": 1,
			        "shadowAlpha": 0
			    },
			    "graphs": [{
			        "id": "g1",
			        "balloon":{
			          "adjustBorderColor":false,
			          "color":"#ffffff"
			        },
			        "bullet": "round",
			        "bulletBorderAlpha": 1,
			        "bulletColor": "#FFFFFF",
			        "bulletSize": 5,
			        "hideBulletsCount": 50,
			        "lineThickness": 2,
			        "title": "red line",
			        "useLineColorForBulletBorder": true,
			        "valueField": "profit",
			        "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
			    }],
			    "chartScrollbar": {
			        "graph": "g1",
			        "oppositeAxis":false,
			        "offset":30,
			        "scrollbarHeight": 50,
			        "backgroundAlpha": 0,
			        "selectedBackgroundAlpha": 0.1,
			        "selectedBackgroundColor": "#888888",
			        "graphFillAlpha": 0,
			        "graphLineAlpha": 0.5,
			        "selectedGraphFillAlpha": 0,
			        "selectedGraphLineAlpha": 1,
			        "autoGridCount":true,
			        "color":"#AAAAAA"
			    },
			    "chartCursor": {
			        "pan": true,
			        "valueLineEnabled": true,
			        "valueLineBalloonEnabled": true,
			        "cursorAlpha":1,
			        "cursorColor":"#258cbb",
			        "limitToGraph":"g1",
			        "valueLineAlpha":0.2,
			        "valueZoomable":true
			    },
			    "valueScrollbar":{
			      "oppositeAxis":false,
			      "offset":100,
			      "scrollbarHeight":10
			    },
			    "categoryField": "date",
			    "categoryAxis": {
			        "dashLength": 1,
			        "minorGridEnabled": true
			    },
			    "export": {
			        "enabled": true
			    },
			    "dataProvider": data
			});
		},
		error: function(msg){
			alert("Error: Unable to fetch finance data");
		}
	});

	$('#transactions_filter').change(function(){
		$.ajax({
			type: "get",
			url: "Auth/getTransactionsData",
			dataType: "json",
			data: { 'filter': $(this).val() },
			success: function(data){
				transactionsChart.dataProvider = data;
				transactionsChart.validateData();
			}
		});
	});

	$('#finance_filter').change(function(){
		$.ajax({
			type: "get",
			url: "Auth/getFinanceData",
			dataType: "json",
			data: { 'filter': $(this).val() },
			success: function(data){
				financeChart.dataProvider = data;
				financeChart.validateData();
			}
		});
	});
});
