<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require APPPATH . 'third_party/vendor/autoload.php';

class Pdf {

    private $pdf;

    public function create_pdf($file, $purpose, $form, $id) {
      global $pdf;

      $pdf = new FPDI();
      $pdf->setSourceFile( PATH_PDFS . $file );
      $tplIdx = $pdf->importPage(1);
      $s = $pdf->getTemplatesize($tplIdx);
      $pdf->AddPage('P', array($s['w'], $s['h']));
      $pdf->useTemplate($tplIdx);

      $pdf->AddFont( 'Cambria', '', 'cambria.php' );
      $pdf->AddFont( 'Cambria-Bold', 'B', 'cambria-bold.php' );
      $pdf->AddFont( 'Cambria-Italic', 'I', 'cambria-italic.php' );
      $pdf->AddFont( 'Lucida', '', 'Lucida-it.php' );

      $pdf->SetFont( 'Cambria-Bold', 'B', 12 );

      $current_date = explode("-", date("jS-F-Y"));

      if ( isset($form['house_num']) && isset($form['street_name']) ) {
        $form['location_1'] = $form['address_1'] = $form['address'] = $form['house_num'] . " " . ucwords($form['street_name']);
        $form['location_2'] = $form['address_2'] = "Brgy. Poblacion, Metro Manila";
        $form['address'] .= ', ' . $form['address_2'];
      }

      if ( isset($form['gender']) )
        $form['gender'] = $form['gender'] == 'f' ? 'Female' : 'Male';

      if ( isset($form['full_name']) )
        $form['full_name'] = json_decode(json_encode($form['full_name']), true);

      if ( isset($form['full_name'][1]) )
        $form['owner'] = $form['full_name'][0] . ' and ' . $form['full_name'][1];

      switch( strtolower($purpose) ) {
      	case "bonafide":
      	case "pre-employment":

      		$offset = $this->get_offset( array( $form['last_name'], $form['first_name'], $form['middle_initial'] ), 106 );
      		$pdf->SetXY( 95, 128 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['last_name'] ) * $offset, 1, $form['last_name'], 0, 0, 'C' );
      		$pdf->Cell( $pdf->GetStringWidth( $form['first_name'] ) * $offset, 1, $form['first_name'], 0, 0, 'C' );
      		$pdf->Cell( $pdf->GetStringWidth( $form['middle_initial'] ) * $offset, 1, $form['middle_initial'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address_1'] ), 102 );
      		$pdf->SetXY( 100, 140 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address_1'] ) * $offset, 1, $form['address_1'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address_2'] ), 120 );
      		$pdf->SetXY( 81, 146 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address_2'] ) * $offset, 1, $form['address_2'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['birthdate'] ), 37 );
      		$pdf->SetXY( 110, 152 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['birthdate'] ) * $offset, 1, $form['birthdate'], 0, 0, 'C' );

      		$age = $this->get_age( $form['birthdate'] );
      		$pdf->SetXY( 163, 152 );
      		$pdf->Cell( $pdf->GetStringWidth( (string) $age ), 1, $age, 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['gender'] ), 19 );
      		$pdf->SetXY( 189, 152 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['gender'] ) * $offset, 1, $form['gender'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['purpose'] ), 100 );
      		$pdf->SetXY( 100, 158 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['purpose'] ) * $offset, 1, $form['purpose'], 0, 0, 'C' );

      		$pdf->SetXY( 111, 281 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 29 );
      		$pdf->SetXY( 133, 281 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 166, 281 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "indigency":

      		$offset = $this->get_offset( array( $form['full_name'] ), 70 );
      		$pdf->SetXY( 127, 106 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['full_name'] ) * $offset, 1, $form['full_name'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address_1'] ), 56 );
      		$pdf->SetXY( 104, 112 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address_1'] ) * $offset, 1, $form['address_1'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( ucwords($form['connection']) ), 87 );
      		$pdf->SetXY( 111, 147.5 );
      		$pdf->Cell( $pdf->GetStringWidth( ucwords($form['connection']) ) * $offset, 1, ucwords($form['connection']), 0, 0, 'C' );

      		$pdf->SetXY( 109, 177 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 29 );
      		$pdf->SetXY( 131, 177 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 164, 177 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "special brgy. permit (big)":

      		$offset = $this->get_offset( array( $form['organization'] ), 72 );
      		$pdf->SetXY( 130, 101 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['organization'] ) * $offset, 1, $form['organization'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['event'] ), 96 );
      		$pdf->SetXY( 105, 113 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['event'] ) * $offset, 1, $form['event'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['location'] ), 98 );
      		$pdf->SetXY( 102, 125 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['location'] ) * $offset, 1, $form['location'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['valid_until'] ), 98 );
      		$pdf->SetXY( 102, 136 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['valid_until'] ) * $offset, 1, $form['valid_until'], 0, 0, 'C' );

      		$pdf->SetXY( 150, 160 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 19 );
      		$pdf->SetXY( 174, 160 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 197, 160 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "business clearance":

      		$offset = $this->get_offset( array( $form['business_name'] ), 114 );
      		$pdf->SetXY( 80, 106 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['business_name'] ) * $offset, 1, $form['business_name'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['location_1'] ), 108 );
      		$pdf->SetXY( 85, 123 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['location_1'] ) * $offset, 1, $form['location_1'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['location_2'] ), 97 );
      		$pdf->SetXY( 90, 131 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['location_2'] ) * $offset, 1, $form['location_2'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['full_name'] ), 108 );
      		$pdf->SetXY( 85, 146.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['full_name'] ) * $offset, 1, $form['full_name'], 0, 0, 'C' );

      		$pdf->SetXY( 101, 207 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 27 );
      		$pdf->SetXY( 125, 207 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 157, 207 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "paanyaya":

      		$offset = $this->get_offset( array( $form['to'] ), 52 );
      		$pdf->SetXY( 51, 58 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['to'] ) * $offset, 1, $form['to'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['notice_id'] ), 50 );
      		$pdf->SetXY( 133, 58 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['notice_id'] ) * $offset, 1, $form['notice_id'], 0, 0, 'C' );

      		$pdf->SetXY( 32, 88 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['kay_1'] ), 1, $form['kay_1'], 0, 0, 'C' );

      		$pdf->SetXY( 31.5, 93 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['kay_2'] ), 1, $form['kay_2'], 0, 0, 'C' );

      		$date_1 = $this->format_date( $form['date_1'], 'jS-F-Y' );

      		$pdf->SetXY( 26, 107 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[0] ), 1, $date_1[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $date_1[1] ), 24 );
      		$pdf->SetXY( 50, 107 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[1] ) * $offset, 1, $date_1[1], 0, 0, 'C' );

      		$pdf->SetXY( 77, 107 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[2] ), 1, $date_1[2], 0, 0, 'C' );

      		$date_2 = $this->format_date( $form['date_2'], 'jS-F-Y' );

      		$pdf->SetXY( 116, 107 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_2[0] ), 1, $date_2[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $date_2[1] ), 25 );
      		$pdf->SetXY( 131, 107 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_2[1] ) * $offset, 1, $date_2[1], 0, 0, 'C' );

      		$pdf->SetXY( 60, 116.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );
      	// $offset = 1;
      		$offset = $this->get_offset( array( $current_date[1] ), 27 );
      		$pdf->SetXY( 84, 116.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 112, 116.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      		// second copy
      		$offset = $this->get_offset( array( $form['to'] ), 52 );
      		$pdf->SetXY( 51, 58 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['to'] ) * $offset, 1, $form['to'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['notice_id'] ), 50 );
      		$pdf->SetXY( 133, 58 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['notice_id'] ) * $offset, 1, $form['notice_id'], 0, 0, 'C' );

      		$pdf->SetXY( 32, 88 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['kay_1'] ), 1, $form['kay_1'], 0, 0, 'C' );

      		$pdf->SetXY( 31.5, 93 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['kay_2'] ), 1, $form['kay_2'], 0, 0, 'C' );

      		$date_1 = $this->format_date( $form['date_1'], 'jS-F-Y' );

      		$pdf->SetXY( 26, 107 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[0] ), 1, $date_1[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $date_1[1] ), 24 );
      		$pdf->SetXY( 50, 107 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[1] ) * $offset, 1, $date_1[1], 0, 0, 'C' );

      		$pdf->SetXY( 77, 107 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_1[2] ), 1, $date_1[2], 0, 0, 'C' );

      		$date_2 = $this->format_date( $form['date_2'], 'jS-F-Y' );

      		$pdf->SetXY( 116, 107 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_2[0] ), 1, $date_2[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $date_2[1] ), 25 );
      		$pdf->SetXY( 131, 107 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $date_2[1] ) * $offset, 1, $date_2[1], 0, 0, 'C' );

      		$pdf->SetXY( 60, 116.5 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 27 );
      		$pdf->SetXY( 84, 116.5 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 112, 116.5 + 171.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "special brgy. permit (small)":

      		$offset = $this->get_offset( array( $form['organization'] ), 138 );
      		$pdf->SetXY( 55, 68 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['organization'] ) * $offset, 1, $form['organization'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['event'] ), 150 );
      		$pdf->SetXY( 41, 80 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['event'] ) * $offset, 1, $form['event'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['location'] ), 155 );
      		$pdf->SetXY( 37, 92 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['location'] ) * $offset, 1, $form['location'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['valid_until'] ), 153 );
      		$pdf->SetXY( 38, 103.4 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['valid_until'] ) * $offset, 1, $form['valid_until'], 0, 0, 'C' );

      		$pdf->SetXY( 83, 115);
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );
      		// $offset = 1;
      		$offset = $this->get_offset( array( $current_date[1] ), 26 );
      		$pdf->SetXY( 106, 115 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 138, 115 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      		// second copy
      		$offset = $this->get_offset( array( $form['organization'] ), 138 );
      		$pdf->SetXY( 55, 68 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['organization'] ) * $offset, 1, $form['organization'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['event'] ), 150 );
      		$pdf->SetXY( 41, 80 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['event'] ) * $offset, 1, $form['event'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['location'] ), 155 );
      		$pdf->SetXY( 37, 92 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['location'] ) * $offset, 1, $form['location'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['valid_until'] ), 153 );
      		$pdf->SetXY( 38, 103.4 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $form['valid_until'] ) * $offset, 1, $form['valid_until'], 0, 0, 'C' );

      		$pdf->SetXY( 83, 115 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 26 );
      		$pdf->SetXY( 106, 115 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 138, 115 + 163.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "two people":
      		$complete = "        This is to certify that " . strtoupper( $form['owner'] ) . ", of legal age who currently resides at " . ucwords( $form['address'] ) . ". Living together for almost 2 years up to present.";
          $this->print_complete( $complete, $current_date );
          break;

      	case "business closing":
      		$complete = "        This is to certify that per our records, " . strtoupper( $form['owner'] ) . ", registered owner of " . strtoupper( $form['business'] ) . ", whose business is located at " . ucwords( $form['address'] ) . ", that his/her business was CLOSED since " . $form['year_closed'] . ".";
          $this->print_complete( $complete, $current_date );
          break;

      	case "business ownership":
      		$complete = "        This is to certify that " . strtoupper( $form['owner'] ) . " of legal age who owned the " . strtoupper( $form['business'] ) . " located at " . ucwords( $form['address'] ) . " , from " . $form['from'] . " to " . $form['to'] . ".";
          $this->print_complete( $complete, $current_date );
          break;

      	case "house ownership":
      		$complete = "        This is to certify that " . strtoupper( $form['owner'] ) . " of legal age who owned the house and lot located at " . ucwords( $form['address'] ) . ". Base on papers presented such as: Transfer Certificate of Title & Real Property Tax declaration.";
          $this->print_complete( $complete, $current_date );
          break;

      	case "pcs guardian":
      		$complete = "        This is to certify that " . strtoupper( $form['guardian'] ) . "  is the official guardian of " . strtoupper( $form['applicant']) . ", who currently resides at " . $form['address'] . ". The aforementioned name currently works as a/an " . $form['occupation'] . " ( " . $form['business'] . " ) raises and supports the child's education in lieu of absence of the biological parents.";
          $this->print_pcs( $complete, $current_date, $form );
          break;

      	case "pcs married couple":
      		$complete = "        This is to certify that " . strtoupper( $form['person_1']  ) . " and " . strtoupper( $form['person_2'] ) . " are parents of " . strtoupper( $form['applicant'] ) . " who currently resides at " . ucwords( $form['address'] ) . ".  The aforementioned parents currently work as " . strtoupper( $form['occupation_1'] ) . " and " . strtoupper( $form['occupation_1'] ) . " respectively.";
          $this->print_pcs( $complete, $current_date, $form );
          break;

      	case "pcs single parent":
      		$complete = "        This is to certify that " . strtoupper( $form['parent'] ) . " is the " . strtolower( $form['parent_type'] ) . " of " . strtoupper( $form['applicant'] )  . ", who currently resides at " . ucwords( $form['address'] ) . ".  The aforementioned parent currently works as a/an " . $form['occupation'] . " (  " . $form['business'] . " ) and raises and supports the child all by herself/himself.";
          $this->print_pcs( $complete, $current_date, $form );
          break;

      	case "pcs unemployed":
      		$complete = "        This is to certify that " . strtoupper( $form['parent'] ) . "  who currently resides at " . ucwords( $form['address'] )  . ". The aforementioned parent is a " . strtolower( $form['occupation'] ) . " only and unemployed.";
          $this->print_pcs( $complete, $current_date, $form );
          break;

      	case "pcs residency":
      		$complete = "        This is to certify that " . strtoupper( $form['person_1']  ) . " and " . strtoupper( $form['person_2'] ) . " who currently resides at " . ucwords( $form['address'] ) . ".  The aforementioned PARENTS of " . strtoupper( $form['applicant'] )  . ", currently works as " . strtoupper( $form['occupation_1'] ) . "  ( " . strtoupper( $form['business_1'] ) . " ). His/her earning income is P" . $form['income'] . ".";
          $this->print_pcs( $complete, $current_date, $form );
          break;

        case "residency":
      		$complete = "        This is to certify that " . strtoupper( $form['full_name'] ) . " of legal age currently resides at " . strtoupper( $form['address'] ) . ".";

      		$pdf->SetXY( 25, 146 );
      		$pdf->MultiCell( 165, 5, $complete );

      		$pdf->SetXY( 61, 185.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 26 );
      		$pdf->SetXY( 84, 185.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 114.5, 185.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "death":
      		$date_died = $this->format_date( $form['date_died'], "F-j,-Y" );
      		$complete = "        This is to certify that " . strtoupper( $form['full_name'] ) . ", of legal age died on " . $date_died[0] . " " . $date_died[1] . " " . $date_died[2] . ", is a resident of " . $form['address'] . ".";

          $pdf->SetXY( 25, 156 );
          $pdf->MultiCell( 165, 5, $complete );

          $offset = $this->get_offset( array( $form['applicant'] ), 65 );
          $pdf->SetXY( 127, 177  );
          $pdf->Cell( $pdf->GetStringWidth( $form['applicant'] ) * $offset, 1, $form['applicant'], 0, 0, 'C' );

          $pdf->SetXY( 60, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

          $offset = $this->get_offset( array( $current_date[1] ), 26 );
          $pdf->SetXY( 83, 198.5  );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

          $pdf->SetXY( 114.5, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );
      	 break;

      	case "toda":
      		$complete = "        This is to certify that " . $form['organization'] . " currently operates at " . $form['address'] . ".";
          $this->print_all( $complete, $current_date, $purpose );
          $pdf->SetXY( 25, 156 );
          $pdf->MultiCell( 165, 5, $complete );

          $offset = $this->get_offset( array( $form['applicant'] ), 65 );
          $pdf->SetXY( 127, 177  );
          $pdf->Cell( $pdf->GetStringWidth( $form['applicant'] ) * $offset, 1, $form['applicant'], 0, 0, 'C' );

          $pdf->SetXY( 60, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

          $offset = $this->get_offset( array( $current_date[1] ), 26 );
          $pdf->SetXY( 83, 198.5  );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

          $pdf->SetXY( 114.5, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );
         break;

      	case "als":

      		$offset = $this->get_offset( array( $form['full_name'] ), 62 );
      		$pdf->SetXY( 137, 107  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['full_name'] ) * $offset, 1, $form['full_name'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address'] ), 85 );
      		$pdf->SetXY( 110, 113.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address'] ) * $offset, 1, $form['address'], 0, 0, 'C' );

      		$pdf->SetXY( 111, 186.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 33 );
      		$pdf->SetXY( 138, 186.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 180, 186.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "authorization":
      		$complete1 = "        This is to certify that " . strtoupper( $form['person_1'] ) . ", legal age and residing at " . ucwords( $form['address'] ) . ", authorized his/her " . $form['relationship'] . ", " . strtoupper( $form['person_2'] ) . ", to transact/claim any benefit of Philhealth in his/her behalf.";
      		$complete2 = "";

          $pdf->SetFont( 'Cambria', '', 11 );
      		$pdf->SetXY( 25, 126 );
      		$pdf->MultiCell( 165, 5, $complete1 );

      		$pdf->SetXY( 25, 146 );
      		$pdf->MultiCell( 165, 5, $complete2 );

      		$pdf->SetXY( 25, 166 );
      		$pdf->MultiCell( 165, 5, "        This certification is being issued this " . $current_date[0] . " day of " . $current_date[1] . ", " . $current_date[2] . "." );

      	break;

      	case "blind":
      		$complete1 = "        This is to certify that the " . strtoupper( $form['organization'] ) . ". has been authorized to conduct a " . ucwords( $form['event'] ) . " within Barangay Poblacion.";
      		$complete2 = "        The aforementioned party has been also authorized by the Department of Social Welfare and Development Central Office to conduct the said campaign.";

      		$pdf->SetFont( 'Cambria', '', 11 );
      		$pdf->SetXY( 25, 126 );
      		$pdf->MultiCell( 165, 5, $complete1 );

      		$pdf->SetXY( 25, 146 );
      		$pdf->MultiCell( 165, 5, $complete2 );

      		$pdf->SetXY( 25, 166 );
      		$pdf->MultiCell( 165, 5, "        This certification is being issued this " . $current_date[0] . " day of " . $current_date[1] . ", " . $current_date[2] . "." );

      	break;

      	case "no derogatory":

      		$pdf->SetFont( 'Lucida', '', 13 );

      		$offset = $this->get_offset( array( $form['full_name'] ), 72 );
      		$pdf->SetXY( 85, 127  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['full_name'] ) * $offset, 1, strtoupper( $form['full_name'] ), 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address'] ), 140 );
      		$pdf->SetXY( 32, 133  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address'] ) * $offset, 1, ucwords( $form['address'] ), 0, 0, 'C' );

      		$pdf->SetXY( 54, 210 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 33 );
      		$pdf->SetXY( 83, 210  );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 119, 210 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "good moral":

      		$pdf->SetFont( 'Lucida', '', 13 );

      		$offset = $this->get_offset( array( $form['full_name'] ), 62 );
      		$pdf->SetXY( 137, 132.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['full_name'] ) * $offset, 1, $form['full_name'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['address'] ), 85 );
      		$pdf->SetXY( 110, 139.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['address'] ) * $offset, 1, $form['address'], 0, 0, 'C' );

      		$pdf->SetXY( 110, 212 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 33 );
      		$pdf->SetXY( 140, 212  );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 180, 212 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      	case "file to action":

      		$pdf->SetXY( 26, 72.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['complainant_1'] ), 1, $form['complainant_1'], 0, 0, 'C' );

      		// $pdf->SetXY( 26, 82  );
      		// $pdf->Cell( $pdf->GetStringWidth( $form['complainant_2'] ), 1, $form['complainant_2'], 0, 0, 'C' );

      		$pdf->SetXY( 26, 110.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['respondent_1'] ), 1, $form['respondent_1'], 0, 0, 'C' );

      		// $pdf->SetXY( 26, 120  );
      		// $pdf->Cell( $pdf->GetStringWidth( $form['respondent_2'] ), 1, $form['respondent_2'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['case_no'] ), 33 );
      		$pdf->SetXY( 156, 72.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['case_no'] ) * $offset, 1, $form['case_no'], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $form['for'] ), 54 );
      		$pdf->SetXY( 132, 82  );
      		$pdf->Cell( $pdf->GetStringWidth( $form['for'] ) * $offset, 1, $form['for'], 0, 0, 'C' );

      		$pdf->SetXY( 35, 210.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      		$offset = $this->get_offset( array( $current_date[1] ), 33 );
      		$pdf->SetXY( 54, 210.5  );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      		$pdf->SetXY( 89, 210.5 );
      		$pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );

      	break;

      }

      $pdf->Output(PATH_TRANSACT . $id . '.pdf','F');
    }

    function print_all( $complete, $current_date, $purpose ) {
      global $pdf;

      switch( $purpose ) {
        case 'death':
        case 'toda':
          $pdf->SetXY( 25, 156 );
          $pdf->MultiCell( 165, 5, $complete );

          $offset = $this->get_offset( array( $form['applicant'] ), 65 );
          $pdf->SetXY( 127, 177  );
          $pdf->Cell( $pdf->GetStringWidth( $form['applicant'] ) * $offset, 1, $form['applicant'], 0, 0, 'C' );

          $pdf->SetXY( 60, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

          $offset = $this->get_offset( array( $current_date[1] ), 26 );
          $pdf->SetXY( 83, 198.5  );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

          $pdf->SetXY( 114.5, 198.5 );
          $pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );
        break;
      }
    }

    function print_pcs( $complete, $current_date, $form ) {
      global $pdf;

      $pdf->SetXY( 25, 137 );
      $pdf->MultiCell( 165, 5, $complete );

      $offset = $this->get_offset( array( $form['applicant'] ), 45 );
      $pdf->SetXY( 93, 176.5  );
      $pdf->Cell( $pdf->GetStringWidth( $form['applicant'] ) * $offset, 1, $form['applicant'], 0, 0, 'C' );

      $pdf->SetXY( 61, 185.5 );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      $offset = $this->get_offset( array( $current_date[1] ), 26 );
      $pdf->SetXY( 84, 185.5  );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      $pdf->SetXY( 114.5, 185.5 );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );
    }

    function print_complete( $complete, $current_date ) {
      global $pdf;

      $pdf->SetXY( 25, 162 );
      $pdf->MultiCell( 165, 5, $complete );

      $pdf->SetXY( 60.5, 207.5 );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[0] ), 1, $current_date[0], 0, 0, 'C' );

      $offset = $this->get_offset( array( $current_date[1] ), 26 );
      $pdf->SetXY( 84, 207.5  );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[1] ) * $offset, 1, $current_date[1], 0, 0, 'C' );

      $pdf->SetXY( 115, 207.5 );
      $pdf->Cell( $pdf->GetStringWidth( $current_date[2] ), 1, $current_date[2], 0, 0, 'C' );
    }

    function set_proper_font( $style, $string, $field_length ) {
    	global $pdf;

    	switch ( $style ) {
    		case "I": $font = "Cambria-Italic"; break;
    		case "B": $font = "Cambria-Bold"; break;
    		default: $font = "Cambria"; break;
    	}

    	if ( $pdf->GetStringWidth( $string ) > $field_length )
    		$pdf->SetFont( $font, $style, 10 );
    	else
    		$pdf->SetFont( $font, $style, 12 );
    }

    function format_date( $date, $format ) {
    	$date = new DateTime( $date );
    	return explode("-", $date->format( $format ));
    }

    function get_age( $birthdate ) {

    	$birthDate = explode("-", $birthdate);

    	$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[2], $birthDate[0]))) > date("md")
    		? ((date("Y") - $birthDate[0]) - 1)
    		: (date("Y") - $birthDate[0]));
    	return $age;
    }

    function get_offset( $inputs, $field_length ) {
    	global $pdf;

    	$sum = 0;

    	foreach ($inputs as $input) {
    		$sum += $pdf->GetStringWidth( $input );
    	}

    	return $field_length / $sum;
    }

}
