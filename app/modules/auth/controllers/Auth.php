<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Auth_model', 'auth');
    }

    public function index()
    {
        if ( $this->input->get('login') != null )
          redirect('');
        
        if( EMPTY($this->session->userdata('username')) )
        {
          $this->layouts->view('login', array(), 'template_login');
        }
        else
        {
          $data['cards_transaction'] = $this->auth->get_cards_transactions()[0];
          $data['cards_user'] = $this->auth->get_cards_users()[0];
          $this->layouts->view('dashboard', $data);
        }
    }

    public function login() {
      $status = 'error';

      $username = $this->input->post('username');
      $password = $this->input->post('password');

      if ( $username == 'admin' && $password == 'admin' ) {
          $this->session->set_userdata( array('username' => $username) );
          $status = 'success';
      }

      header('Location: ' . base_url() . 'auth?login=' . $status);
    }

    public function logout() {
      $this->session->sess_destroy();
      redirect('');
    }

    public function view_tables()
    {
        if ($this->input->get('login') != null )
          redirect('');
        
        if( EMPTY($this->session->userdata('username')) )
        {
          $this->layouts->view('login', array(), 'template_login');
        }
        else
        {
          $this->layouts->view('tables');
        }
    }


    public function getTransactionsData(){
      $param = "";
      if ( $this->input->get('filter') !== null )
        $param = $this->input->get('filter');
      echo json_encode($this->auth->get_count_transactions($param), JSON_NUMERIC_CHECK);
    }

    public function getFinanceData(){
      $param = "";
      if ( $this->input->get('filter') !== null )
        $param = $this->input->get('filter');
      echo json_encode($this->auth->get_transaction_profit($param), JSON_NUMERIC_CHECK);
    }
}
