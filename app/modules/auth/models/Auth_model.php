<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends DBMAIN
{

    public function get_count_transactions($param)
    {
      switch($param) {
        case 'daily':
        case '':
          $select_date = "DATE_FORMAT(transactions.date_issued, '%b %c, %Y')";
          break;
        case 'monthly':
          $select_date = "CONCAT(MONTHNAME(transactions.date_issued), ' ', REPLACE(extract(year from transactions.date_issued), ',', ''))";
          break;

        case 'yearly':
          $select_date = "replace(extract(year from transactions.date_issued), ',', '')";
          break;
      }

      $this->db->select($select_date . " as date, count(case when doc_types.name = 'clearance' then 1 else null end) as clearances, count(case when doc_types.name = 'certificate' then 1 else null end) as certificates");
      $this->db->from("transactions");
      $this->db->join("doc_types", "transactions.doc_type_id = doc_types.id");
      $this->db->group_by("date");
      $query = $this->db->get();

      return $query->result_array();

    }

    public function get_transaction_profit($param){
      switch($param) {
        case 'daily':
        case '':
          $select_date = "DATE_FORMAT(transactions.date_issued, '%b %c, %Y')";
          break;
        case 'monthly':
          $select_date = "CONCAT(MONTHNAME(transactions.date_issued), ' ', REPLACE(extract(year from transactions.date_issued), ',', ''))";
          break;

        case 'yearly':
          $select_date = "replace(extract(year from transactions.date_issued), ',', '')";
          break;
      }

    	$this->db->select($select_date . " as date, SUM(price) as profit");
    	$this->db->from("transactions");
    	$this->db->group_by("date");
		  $query = $this->db->get();

		  return $query->result_array();
    }

    public function get_cards_transactions() {
      $this->db->select('COALESCE(SUM(CASE WHEN date_issued = CURRENT_DATE THEN price ELSE 0 END), 0) as profit, count(*) as total,
TRUNCATE(COALESCE((SUM(CASE WHEN date_issued = CURRENT_DATE THEN price ELSE 0 END) - SUM(CASE WHEN date_issued = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) THEN price ELSE 0 END)) /
SUM(CASE WHEN date_issued = CURRENT_DATE THEN price ELSE 0 END) * 100, 0), 2) as percent_profit,
TRUNCATE(COALESCE((SUM(CASE WHEN date_issued = CURRENT_DATE THEN 1 ELSE 0 END) - SUM(CASE WHEN date_issued = DATE_SUB(CURRENT_DATE, INTERVAL 1 DAY) THEN 1 ELSE 0 END)) /
SUM(CASE WHEN date_issued = CURRENT_DATE THEN 1 ELSE 0 END) * 100, 0), 2) as percent_total');
      $this->db->where('date_issued', 'CURRENT_DATE');
      $query = $this->db->get('transactions');

      return $query->result();
    }

    public function get_cards_users() {
      $this->db->select("SUM(CASE WHEN (YEAR(CURRENT_DATE) - YEAR(birthdate) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthdate, '%m%d'))) < 18 THEN 1 ELSE 0 END) as below_18,
SUM(CASE WHEN (YEAR(CURRENT_DATE) - YEAR(birthdate) - (DATE_FORMAT(CURRENT_DATE, '%m%d') < DATE_FORMAT(birthdate, '%m%d'))) > 60 THEN 1 ELSE 0 END) as above_60");
      $query = $this->db->get("users");

      return $query->result();
    }

}