<div class="container">
  <div class="row charts-row">

    <div class="col s12 m12 l6">
      <div class="card teal lighten-5">
        <div class="card-content">
          <h5>Transactions</h5>
          <select id="transactions_filter" class="browser-default" name="">
            <option value="daily" selected>Daily</option>
            <option value="monthly">Monthly</option>
            <option value="yearly">Yearly</option>
          </select>
         <div class="chart" id="transactionsChart"></div>
        </div>
      </div>
    </div>

    <div class="col s12 m12 l6">
      <div class="card teal lighten-5">
        <div class="card-content">
          <h5>Finance</h5>
          <select id="finance_filter" class="browser-default" name="">
            <option value="daily" selected="">Daily</option>
            <option value="monthly">Monthly</option>
            <option value="yearly">Yearly</option>
          </select>

         <div class="chart" id="financeChart"></div>
        </div>
      </div>
    </div>
</div>
<div class="row row-cards">
    <div class="col s12 m6 l3">
        <div class="card">
            <div class="card-content blue-grey white-text">
                <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Today Profit</p>
                <h4 class="card-stats-number">Php <?php echo $cards_transaction->profit; ?></h4>
                <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i> <?php echo $cards_transaction->percent_profit; ?> <span class="blue-grey-text text-lighten-5">from yesterday</span>
                </p>
            </div>
        </div>
    </div>

    <div class="col s12 m6 l3">
        <div class="card">
            <div class="card-content blue-grey white-text">
                <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Today Transactions</p>
                <h4 class="card-stats-number"><?php echo $cards_transaction->total; ?></h4>
                <p class="card-stats-compare"><i class="mdi-hardware-keyboard-arrow-up"></i> <?php echo $cards_transaction->percent_total; ?> <span class="blue-grey-text text-lighten-5">from yesterday</span>
                </p>
            </div>
        </div>
    </div>

    <div class="col s12 m6 l3">
        <div class="card">
            <div class="card-content blue-grey white-text">
                <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Residents below 18</p>
                <h4 class="card-stats-number"><?php echo $cards_user->below_18; ?></h4>
            </div>
        </div>
    </div>

    <div class="col s12 m6 l3">
        <div class="card">
            <div class="card-content blue-grey white-text">
                <p class="card-stats-title"><i class="mdi-action-trending-up"></i> Residents above 60</p>
                <h4 class="card-stats-number"><?php echo $cards_user->above_60; ?></h4>
            </div>
        </div>
    </div>
  </div>
</div>