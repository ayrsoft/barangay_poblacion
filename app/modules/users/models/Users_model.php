<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends DBMAIN
{

    public function insert_address_data($addressData){
        $this->db->insert("address", $addressData);
        $last_id = $this->db->insert_id();
        return $last_id;
    }

    public function insert_user_data($userData){
        $this->db->insert("users", $userData);
    }

    public function select_civil_list(){
        $this->db->select('id, name');
        $this->db->from('civil_status');

        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_user_by_id($id) {
      // TODO:
      // Format users.birthdate: 16 Sept, 2017
      $this->db->select('users.id, users.first_name, users.middle_name, users.last_name, users.vin_no, users.email, users.gender, users.civil_status_id, users.contact_id, users.citizenship, address.house_num, address.street_name, users.birthdate, users.birthplace, users.occupation, users.weight, users.height');
      $this->db->from(DBMAIN::tbl_users);
      $this->db->join(DBMAIN::tbl_address, DBMAIN::tbl_address . '.id = ' . DBMAIN::tbl_users . '.address_id');
      $this->db->where(DBMAIN::tbl_users . '.id', $id);
      $this->db->where(DBMAIN::tbl_users . '.active_flag', 1);
      $query = $this->db->get();

      return $query->result();
    }

    public function update_user_by_id($user_data, $id) {
      $this->db->set($user_data);
      $this->db->where('id', $id);
      $this->db->update(DBMAIN::tbl_users);

      if ( $this->db->affected_rows() != 0 )
        return "success";
      else
        return "error";
    }

    public function delete_user($id) {
      $this->db->set('active_flag', 0);
      $this->db->where('id', $id);
      $query = $this->db->update(DBMAIN::tbl_users);

      if ( $this->db->affected_rows() != 0 )
        return "success";
      else
        return "error";
    }
}
