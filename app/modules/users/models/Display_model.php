<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Display_model extends DBMAIN
{
    public function get_users(){
        $this->db->select('users.id, users.first_name, users.middle_name, users.last_name, users.vin_no, users.email, IF(users.gender=\'m\',\'Male\', \'Female\') as gender, users.civil_status_id, users.contact_id, users.citizenship, address.house_num, address.street_name, users.birthdate, users.birthplace, users.occupation, users.weight, users.height, civil_status.name as civil_status');
        $this->db->from(DBMAIN::tbl_users);
        $this->db->join(DBMAIN::tbl_address, DBMAIN::tbl_address . '.id = ' . DBMAIN::tbl_users . '.address_id');
        $this->db->join(DBMAIN::tbl_civil_status, DBMAIN::tbl_civil_status . '.id = ' . DBMAIN::tbl_users . '.civil_status_id');
        $this->db->where(DBMAIN::tbl_users . '.active_flag', 1);
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_user($id){
    	$this->db->select('*');
      $this->db->from('users');
      $this->db->where('id', $id);
      $query = $this->db->get();
      return $query->result_array();
    }

}
