<div class="col s12 m12 l6">
	<div class="card-panel">
		<h4 class="header">User's Information</h4>
		<div class="row">
			<form class="col s12" method="post" id="update-form">
        <input type="hidden" name="id" value="<?php echo $user->id; ?>">
				<div class="row">
					<div class="input-field col s4">
						<input placeholder="John" id="first_name" name="first_name" type="text" required value="<?php echo $user->first_name; ?>">
						<label class="active" for="first_name">Firstname</label>
					</div>
					<div class="input-field col s4">
						<input placeholder="Santos" id="middle_name" name="middle_name" type="text" required value="<?php echo $user->middle_name; ?>">
						<label class="active" for="middle_name">Middlename</label>
					</div>
					<div class="input-field col s4">
						<input placeholder="Doe" id="last_name" name="last_name" type="text" required value="<?php echo $user->last_name; ?>">
						<label class="active" for="last_name">Lastname</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s5">
						<input placeholder="7606-0001A-K0880HBA100004" id="vin_no" name="vin_no" type="text" required value="<?php echo $user->vin_no; ?>">
						<label class="active" for="vin_no">VIN no</label>
					</div>
					<div class="input-field col s5">
						<input placeholder="John_Doe@gmail.com" id="email" name="email" type="text" required value="<?php echo $user->email; ?>">
						<label class="active" for="email">Email Address</label>
					</div>
					<div class="input-field col s2">
						<select id="gender" name="gender">
							<option value="M" <?php echo ($user->gender =='m' ? 'selected' : ''); ?>>Male</option>
							<option value="F" <?php echo ($user->gender == 'f' ? 'selected' : ''); ?>>Female</option>
						</select>
						<label for="gender">Gender</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s4">
						<select id="civil_status" name="civil_status">
						<?php for ($i=0; $i <count($civil_list); $i++) {
							echo "<option value=" . $civil_list[$i]['id'] . " " . ($user->civil_status_id == $civil_list[$i]['id'] ? 'selected' : '') . ">".$civil_list[$i]['name']."</option>";
						} ?>
						</select>
						<label for="civil_status">Civil Status</label>
					</div>
					<div class="input-field col s4">
						<input placeholder="09123456789" id="contact_id" name="contact_id" type="number" required value="<?php echo $user->contact_id; ?>">
						<label class="active" for="contact_no">Contact no</label>
					</div>
					<div class="input-field col s4">
						<input placeholder="Filipino" id="citizenship" name="citizenship" type="text" required value="<?php echo $user->citizenship; ?>">
						<label class="active" for="citizenship">Citizenship</label>
					</div>
				</div>
				<div class="row">
					<!-- <p><h6>Address</h6></p> -->
					<div class="input-field col s3">
						<input placeholder="020 E." id="house_num" name="house_num" type="number" required value="<?php echo $user->house_num; ?>">
						<label class="active" for="house_num">House no</label>
					</div>
					<div class="input-field col s5">
						<input placeholder="QUIOGEU ST., AGUHO" id="street_name" name="street_name" type="text" required value="<?php echo $user->street_name; ?>">
						<label class="active" for="street_name">Street name</label>
					</div>
					<div class="input-field col s4">
						<input id="birth_date" name="birth_date" type="date" class="datepicker" required value="<?php echo $user->birthdate; ?>">
						<label class="active" for="birth_date">Birthdate</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s4">
						<input placeholder="Sampaloc, Manila" id="birth_place" name="birth_place" type="text" required value="<?php echo $user->birthplace; ?>">
						<label class="active" for="birth_place">Birthplace</label>
					</div>
					<div class="input-field col s4">
						<input placeholder="Programmer" id="occupation" name="occupation" type="text" required value="<?php echo $user->occupation; ?>">
						<label class="active" for="occupation">Occupation</label>
					</div>
					<div class="input-field col s2">
						<input placeholder="55" id="weight" name="weight" type="number" value="<?php echo $user->weight; ?>">
						<label class="active" for="weight">Weight</label>
					</div>
					<div class="input-field col s2">
						<input placeholder="6'1" id="height" name="height" type="text" value="<?php echo $user->height; ?>">
						<label class="active" for="height">Height</label>
					</div>
				</div>
				<!-- <div class="row"> -->
					<!-- <div class="input-field col s4">
		               <select id="life" name="life" onchange="isLife(this.value)">
							<option value="alive">Alive</option>
							<option value="dead">Dead</option>
						</select>
						<label for="life">Current Status</label>
					</div> -->
					<!-- <div class="input-field col s6" id="dead" style="display: none;">
						<input id="death_date" name="death_date" type="date" class="datepicker" required>
						<label for="death_date">Death Date</label>
					</div> -->
				<!-- </div> -->
				<div class="row">
					<div class="input-field col s12">
						<button class="btn cyan waves-effect waves-light right" type="button" name="action" id="updateButton">Update
							<i class="mdi-content-send right"></i>
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
