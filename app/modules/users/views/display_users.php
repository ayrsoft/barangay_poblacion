<div class="container">
	<div class="row">
		<h4 class="header">List of Residents</h4>
	</div>

	<div class="row container">
			<table id="data_table_users" class="responsive-table display" cellspacing="0">
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<tr id="remove"><td></td><td></td><td></td></tr>
					<?php
					foreach ($user_data as $value) {
						echo '
						<tr>
							<td>'.$value['id'].'</td>
							<td>'.$value['first_name'].' '.$value['last_name'].'<td>
							<td>
								<a class="waves-effect waves-light btn modal-trigger light-green" href="#display_modal_' . $value['id'] . '">View</a>
								<a class="waves-effect waves-light btn light-blue" href="update_user?id=' . $value['id'] . '">Update</a>
								<a class="waves-effect waves-light btn light-red delete-user" id="' . $value['id'] . '">Delete</a>
							</td>
						</tr>
						';
						?>
						<!-- TODO: -->
						<!-- OPTIMIZE PLS -->
						<!-- This is the modal to be shown when clicking View -->
						<div id="display_modal_<?php echo $value['id']; ?>" class="modal">
							<div class="modal-content">
								<div id="profile-card" class="card">
									<div class="card-image waves-effect waves-block waves-light">
										<img class="activator" src="https://image.freepik.com/free-vector/abstract-background-with-a-3d-pattern_1319-68.jpg" alt="user bg">
									</div>
									<div class="card-content">
										<a class="btn-floating activator btn-move-up waves-effect waves-light darken-2 right">
											<i class="mdi-navigation-expand-more"></i>
										</a>

										<span class="card-title activator grey-text text-darken-4"> <?php echo $value['first_name'] . ' ' . $value['middle_name'] . ' ' . $value['last_name']; ?></span>
										<p><i class="mdi-action-perm-identity"></i> <?php echo $value['occupation']; ?></p>
										<p><i class="mdi-action-perm-phone-msg"></i> <?php echo $value['contact_id']; ?></p>
										<p><i class="mdi-communication-email"></i> <?php echo $value['email']; ?></p>

									</div>
									<div class="card-reveal">
										<span class="card-title grey-text text-darken-4">Roger Waters <i class="mdi-navigation-close right"></i></span>
										<p>Here is some more information about this card.</p>
										<p><i class="mdi-action-perm-identity"></i> <?php echo $value['occupation']; ?></p>
										<p><i class="mdi-action-perm-phone-msg"></i> <?php echo $value['contact_id']; ?></p>
										<p><i class="mdi-communication-email"></i> <?php echo $value['email']; ?></p>
										<p><i class="mdi-social-cake"></i> <?php echo date_format(date_create($value['birthdate']),"d F, Y"); ?></p>
										<p><i class="mdi-action-home"></i> <?php echo $value['house_num'] . ' ' . $value['street_name'] . ', Brgy. Poblacion, Metro Manila'; ?></p>
										<p><i class="mdi-content-flag"></i> <?php echo $value['citizenship']; ?></p>
										<p>VIN No.: <?php echo $value['vin_no']; ?></p>
										<p>Gender: <?php echo $value['gender']; ?></p>
										<p>Civil Status: <?php echo $value['civil_status']; ?></p>
										<p>Birthplace: <?php echo $value['birthplace']; ?></p>
										<p>Weight: <?php echo $value['weight']; ?> kg</p>
										<p>Height: <?php echo $value['height']; ?> ft</p>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Close</a>
							</div>
						</div>
						<?php
					}
					?>
				</tbody>
			</table>
	</div>
</div>
