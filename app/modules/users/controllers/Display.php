<?php
/*
 * Health Links Team
 * 
 * Author: Patricia Ayrah Otero
 * 
 * 
 */

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Display extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
          $this->load->model('Display_model','display_users');
    }
    public function index()
    {
        $user_data = $this->display_users->get_users();
        $data['user_data'] = $user_data;
        $this->layouts->view('display_users', $data);
    }

    public function updateUser()
    {
        $user_data = $this->display_users->get_user($_POST['id']);
        $data['user_data'] = $user_data;
        $this->layouts->view('update_user', $data);   
    }   
}