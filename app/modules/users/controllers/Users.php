<?php
// /users/insertUser
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MX_Controller
{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('Users_model','users');
    }

    public function index()
    {
        $civillist = $this->users->select_civil_list();
        $data['civil_list'] = $civillist;
        $this->layouts->view('form', $data);
    }

    public function getCivil_Status_ID(){
        $id = 0;
        $civil_status = array(" ", "Single", "Married", "Widowed", "Separated", "Divorced");
        for ($i=0; $i <count($civil_status); $i++) {
            if($civil_status[$i] == $_POST['civil_status']){
                $id = $i;
                break;
            }
        }
        return $id;
    }

    public function getAddressID(){
        return $this->users->getAddressID();
    }

    public function insertUser() {
        $addressData = array(
            'house_num' => $_POST['house_num'],
            'street_name' => $_POST['street_name']
            );
        $address_id = 0; $death_date = "0000-00-00";
        $address_id = $this->users->insert_address_data($addressData);
        // if($_POST['life'] == "dead"){
        //     $death_date = date('Y-m-d', strtotime(str_replace('-', '/', $_POST['death_date'])));
        // }
        $userData = array(
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'middle_name' => $_POST['middle_name'],
            'vin_no' => $_POST['vin_no'],
            'birthdate' => date('Y-m-d', strtotime(str_replace('-', '/', $_POST['birth_date']))),
            'gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'civil_status_id' => $_POST['civil_status'],
            'contact_id' => $_POST['contact_id'],
            'citizenship' => $_POST['citizenship'],
            'address_id' =>$address_id,
            'birthplace' => $_POST['birth_place'],
            'occupation' => $_POST['occupation'],
            'weight' => $_POST['weight'],
            'height' => $_POST['height']
            );
        $this->users->insert_user_data($userData);
    }

    public function update_user() {
      $user_id = $this->input->get('id');

      $data['user'] = $this->users->get_user_by_id($user_id)[0];
      $data['civil_list'] = $this->users->select_civil_list();

      $this->layouts->view('update_user', $data);
    }

    public function update_user_by_id() {
      $user_id = $this->input->get('id');
      $addressData = array(
          'house_num' => $_POST['house_num'],
          'street_name' => $_POST['street_name']
          );
      $address_id = 0; $death_date = "0000-00-00";
      $address_id = $this->users->insert_address_data($addressData);
      // if($_POST['life'] == "dead"){
      //     $death_date = date('Y-m-d', strtotime(str_replace('-', '/', $_POST['death_date'])));
      // }
      $userData = array(
          'first_name' => $_POST['first_name'],
          'last_name' => $_POST['last_name'],
          'middle_name' => $_POST['middle_name'],
          'vin_no' => $_POST['vin_no'],
          'birthdate' => date('Y-m-d', strtotime(str_replace('-', '/', $_POST['birth_date']))),
          'gender' => $_POST['gender'],
          'email' => $_POST['email'],
          'civil_status_id' => $_POST['civil_status'],
          'contact_id' => $_POST['contact_id'],
          'citizenship' => $_POST['citizenship'],
          'address_id' =>$address_id,
          'birthplace' => $_POST['birth_place'],
          'occupation' => $_POST['occupation'],
          'weight' => $_POST['weight'],
          'height' => $_POST['height']
          );
      echo $this->users->update_user_by_id($userData, $this->input->post('id'));
    }

    public function delete_user() {
      echo $this->users->delete_user($this->input->post('id'));
    }
}
