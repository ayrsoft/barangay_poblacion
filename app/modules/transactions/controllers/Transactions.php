  <?php

    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Transactions extends MX_Controller
    {
        public function __construct()
        {
            parent::__construct();

            $this->load->model('Transactions_model','transact');
            $this->load->model('Users_model', 'residents');
            $this->load->model('Addresses_model', 'address');

            $this->load->library('Pdf', 'pdf');
        }

        public function index()
        {
          $this->layouts->view('index');
        }

        /*
        Should this be get_from_database(column); ?
        */
        public function get_doc_types() {
          echo json_encode( $this->transact->get_doc_types() );
        }

        public function get_residents() {
          echo json_encode( $this->residents->get_residents() );
        }

        public function get_street_names() {
          echo json_encode( $this->address->get_streets() );
        }

        public function save_transactions() {
          // Save to database
          $postdata = file_get_contents("php://input");
          $request = (array) json_decode($postdata);
          unset($request["undefined"]);


          $ret = $this->transact->save_transactions($request);

          if ( $ret['status'] == 'success' ) {
            // Create PDF
            $ctr = -1;
            foreach ($request as $req) {
              ++$ctr;
              $form = $req;
              $doc_type_data = $this->transact->get_doc_type_by_id($form->doc_type_id);
              $purpose =  $doc_type_data[0]->title;
              $file = $doc_type_data[0]->file_name;
              $form = (array) $form;
              $this->pdf->create_pdf($file, $purpose, $form, $ret['ids'][$ctr]);
            }
          }

          echo json_encode($ret);
        }

        public function view_transactions() {
          $data['clearances'] = $this->transact->get_transactions( 'clearance' );
          $data['certificates'] = $this->transact->get_transactions( 'certificate' );
          $this->layouts->view('view', $data);
        }

    }
