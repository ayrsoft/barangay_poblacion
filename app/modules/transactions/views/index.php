<div ng-controller="TransactionsCtrl as transact" class="sample" layout="column" ng-cloak>
  <md-content class="md-padding">
    <md-tabs md-selected="selectedIndex" md-border-bottom md-autoselect>
      <md-tab ng-repeat="tab in tabs"
              ng-disabled="tab.disabled"
              label="{{tab.document.title}}">

        <div class="demo-tab tab{{$index%4}}" style="padding: 25px; text-align: center;">
          <div class="row">
            <div class="col s12 m12 l12">
              <div class="card-panel">
                <div class="row">
                  <form class="col s12" id="tab{{$index%4}}" name="transact.forms[tab{{$index%4}}]">
                    <div ng-init="tab_name = 'tab' + $index%4; transact.forms[tab_name].doc_type_id = tab.document.id" ng-include="'./assets/forms/' + tab.document.layout" onload="init_datepicker(tab_name)"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>

          <br/>
          <md-button class="md-primary md-raised" ng-click="removeTab( tab )" ng-disabled="tabs.length <= 1">Remove Tab</md-button>
        </div>
      </md-tab>
    </md-tabs>
  </md-content>

  <form ng-submit="addTab(tTitle)" layout="column" class="md-padding" style="padding-top: 0;">
    <div layout="row" layout-sm="column">
      <div flex style="position: relative;">
        <md-button ng-click="submitForms(tab)" class="submit-forms md-primary md-raised" ng-disabled="tabs.length < 1" type="button" style="margin-right: 0;">Submit All</md-button>
        <!-- Modal trigger -->
      </div>
      <md-input-container>
        <select class="input-field browser-default" ng-options="document.title for document in documents" ng-model="tTitle">
            <option value="" disabled selected>Select document</option>
        </select>
      </md-input-container>
      <md-button class="add-tab md-primary md-raised" ng-disabled="!tTitle" type="submit" style="margin-right: 0;">Add Tab</md-button>
    </div>
  </form>
</div>
