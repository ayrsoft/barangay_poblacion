<div class="col s12">
    <div class="tab-container col s12">
      <ul class="tabs tab-demo-active z-depth-1 cyan">
        <li class="tab col s3"><a class="white-text waves-effect waves-light active" href="#certificate">CERTIFICATES</a>
        </li>
        <li class="tab col s3"><a class="white-text waves-effect waves-light" href="#clearance">CLEARANCES</a>
        </li>
      </ul>
    </div>

    <div class="col s12">
      <div id="certificate" class="transaction-container col s12  cyan lighten-4">
        <div class="col s12">
          <table id="data_table_certificate" class="data-table-simple responsive-table display" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Applicant</th>
                <th>Date Created</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              <?php foreach ($certificates as $certificate): ?>
                <tr>
                  <td><?php echo $certificate->id; ?></td>
                  <td><?php echo $certificate->title; ?></td>
                  <td><?php echo $certificate->first_name . ' ' . $certificate->last_name; ?></td>
                  <td><?php echo $certificate->date_issued; ?></td>
                  <td><a href="<?php echo BASE_URL . 'pdfs/' . $certificate->id . '.pdf'; ?>" target="_blank">View</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

    <div class="col s12">
      <div id="clearance" class="transaction-container col s12  cyan lighten-4">
        <div class="col s12">
          <table id="data_table_clearance" class="data-table-simple responsive-table display" cellspacing="0">
            <thead>
              <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Applicant</th>
                <th>Date Created</th>
                <th></th>
              </tr>
            </thead>

            <tbody>
              <?php foreach ($clearances as $clearance): ?>
                <tr>
                  <td><?php echo $clearance->id; ?></td>
                  <td><?php echo $clearance->title; ?></td>
                  <td><?php echo $clearance->first_name . ' ' . $clearance->last_name; ?></td>
                  <td><?php echo $clearance->date_issued; ?></td>
                  <td><a href="<?php echo BASE_URL . 'pdfs/' . $clearance->id . '.pdf'; ?>" target="_blank">View</a></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
</div>
