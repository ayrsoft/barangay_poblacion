<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends DBMAIN
{

    public function get_residents()
    {
      /*
      TODO: specify all columns with proper column names in SELECT
      */
      $this->db->select('*');
      $this->db->from(DBMAIN::tbl_users);
      $this->db->join(DBMAIN::tbl_address, DBMAIN::tbl_address . ".id = " . DBMAIN::tbl_users . ".address_id");
      $this->db->join(DBMAIN::tbl_civil_status, DBMAIN::tbl_civil_status . ".id = " . DBMAIN::tbl_users . ".civil_status_id");
      $this->db->where(DBMAIN::tbl_users . '.active_flag', 1);
      $query = $this->db->get();
      return $query->result();
    }

}
