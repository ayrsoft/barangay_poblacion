<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions_model extends DBMAIN
{

    public function get_doc_types()
    {
        $query = $this->db->get(DBMAIN::tbl_doc_types);
        return $query->result();
    }

    public function save_transactions($data) {

      $ret = array(
        'status' => 'error',
        'ids' => array()
      );

      foreach ($data as $item) {
        foreach ($item->user_id as $user_id) {
          $insert_data = array(
            'doc_type_id' => $item->doc_type_id,
            'user_id' => $user_id,
            'price' => $item->price,
            'date_issued' => date("Y-m-d"),
            'contents' => json_encode($item)
          );

          $this->db->insert(DBMAIN::tbl_transactions, $insert_data);
        }

        $ret['status'] = $this->db->affected_rows() != 1 ? 'error' : 'success';
        array_push($ret['ids'], $this->db->insert_id());
      }

      return $ret;
    }

    public function get_doc_type_by_id($id) {
      $this->db->select('title, file_name');
      $this->db->where('id', $id);
      $query = $this->db->get(DBMAIN::tbl_doc_types);
      return $query->result();
    }

    public function get_transactions( $name ) {
      $this->db->select('transactions.id, doc_types.title, COALESCE(users.first_name, "NA") as first_name, COALESCE(users.last_name, "NA") as last_name, transactions.date_issued');
      $this->db->from(DBMAIN::tbl_transactions);
      $this->db->join(DBMAIN::tbl_users, DBMAIN::tbl_users . '.id = '. DBMAIN::tbl_transactions . '.user_id', 'left');
      $this->db->join(DBMAIN::tbl_doc_types, DBMAIN::tbl_doc_types . '.id = ' . DBMAIN::tbl_transactions . '.doc_type_id');
      $this->db->where(DBMAIN::tbl_doc_types . '.name', $name);
      $query = $this->db->get();
      return $query->result();
    }

}
