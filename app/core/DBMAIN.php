<?php

	if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class DBMAIN extends CI_Model
	{
		const tbl_users								= 'users';
		const tbl_doc_types						= 'doc_types';
		const tbl_address							= 'address';
		const tbl_civil_status				= 'civil_status';
		const tbl_transactions			  = 'transactions';
	}
