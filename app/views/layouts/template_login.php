<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Barangay Poblacion | Automated System</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo PATH_IMG ?>favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo PATH_IMG ?>favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo PATH_IMG ?>favicon/mstile-144x144.png">
    <!-- For Windows Phone -->

    <!-- CORE CSS-->
    <link href="<?php echo PATH_CSS ?>materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_CSS ?>style.min.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- Custom CSS-->
    <link href="<?php echo PATH_CSS ?>custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_CSS ?>custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_CSS ?>/layouts/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo PATH_JS_PLUGIN ?>prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_JS_PLUGIN ?>perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">

    <script>var base_url = '<?php echo BASE_URL ?>';</script>
  </head>

  <body class="cyan">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <div id="login-page" class="row">
      <div class="col s12 z-depth-4 card-panel">
        <form class="login-form" method="post" action="/barangay_poblacion/auth/login">
          <div class="row">
            <div class="input-field col s12 center">
              <!-- TODO: -->
              <!-- Add Brgy's logo  -->
              <!-- <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login"> -->
              <p class="center login-form-text">Brgy. Poblacion Automated System</p>
            </div>
          </div>
          <div class="row margin">
            <div class="input-field col s12">
              <i class="mdi-social-person-outline prefix"></i>
              <input id="username" name="username" type="text">
              <label for="username" class="center-align">Username</label>
            </div>
          </div>
          <div class="row margin">
            <div class="input-field col s12">
              <i class="mdi-action-lock-outline prefix"></i>
              <input id="password" name="password" type="password">
              <label for="password">Password</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <button type="submit" class="btn waves-effect waves-light col s12">Login</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- jQuery Library -->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>materialize.min.js"></script>
    <!--prism -->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>chartist-js/chartist.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>custom-script.js"></script>

    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>data-tables/js/jquery.dataTables.min.js"></script>

  </body>

  </html>
