<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Barangay Poblacion | Automated System</title>

    <!-- Favicons-->
    <link rel="icon" href="<?php echo PATH_IMG ?>favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="<?php echo PATH_IMG ?>favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="<?php echo PATH_IMG ?>favicon/mstile-144x144.png">
    <!-- For Windows Phone -->

    <!-- CORE CSS-->
    <link href="<?php echo PATH_CSS ?>materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_CSS ?>style.min.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- Custom CSS-->
    <link href="<?php echo PATH_CSS ?>custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_CSS ?>custom/custom.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="<?php echo PATH_JS_PLUGIN ?>prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_JS_PLUGIN ?>perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="<?php echo PATH_JS_PLUGIN ?>chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- ANGULAR -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/angular-material/1.1.3/angular-material.css">


    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

    <?php
      $url = $this->uri->segment(1);

      switch($url)
      {
        case '':
          echo "<link href='" . PATH_CSS . "charts-style.css' />";
          echo "<link rel='stylesheet' href='" . PATH_JS . "amcharts/plugins/export/export.css' type='text/css'/>";
          break;

        case 'transactions':
          echo "<link href='//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' rel='stylesheet' />";
          echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">';
          echo '<link href="' . PATH_CSS . 'custom/transactions.css" type="text/css" rel="stylesheet">';
        break;

        default:
        break;
      }
    ?>

    <script>var base_url = '<?php echo BASE_URL ?>';</script>
  </head>

  <body ng-app="app">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
      <div id="loader"></div>
      <div class="loader-section section-left"></div>
      <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- START HEADER -->
    <header id="header" class="page-topbar">
      <!-- start header nav-->
      <div class="navbar-fixed">
        <nav class="navbar-color">
          <div class="nav-wrapper">
            <ul class="left">
              <li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="<?php echo PATH_IMG ?>materialize-logo.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
            </ul>
        </div>
      </nav>
    </div>
    <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation">
          <li class="user-details cyan darken-2">
            <div class="row">
              <div class="col col s4 m4 l4">
                <img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img valign profile-image">
              </div>
              <div class="col col s8 m8 l8">
                <ul id="profile-dropdown" class="dropdown-content">
                  <li><a href="auth/logout"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                  </li>
                </ul>
                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">ADMIN<i class="mdi-navigation-arrow-drop-down right"></i></a>
                <p class="user-roal">Administrator</p>
              </div>
            </div>
          </li>
          <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li class="bold">
                  <a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                    <div class="collapsible-body">
                      <ul>
                        <li><a href="/barangay_poblacion"> Charts</a>
                        <li><a href="/barangay_poblacion/auth/view_tables"> Tables</a>
                      </ul>
                    </div>
                </li>
            </ul>
          </li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                  <li class="bold">
                    <a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-class"></i> Transactions</a>
                      <div class="collapsible-body">
                          <ul>
                              <li><a href="/barangay_poblacion/transactions"> Create</a>
                              </li>
                              <li><a href="/barangay_poblacion/transactions/view_transactions"> Display</a>
                              </li>
                          </ul>
                      </div>
                  </li>
              </ul>
          </li>
          <li class="no-padding">
              <ul class="collapsible collapsible-accordion">
                  <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-perm-identity"></i> Residents</a>
                      <div class="collapsible-body">
                          <ul>
                              <li><a href="/barangay_poblacion/users"> Register</a>
                              </li>
                              <li><a href="/barangay_poblacion/users/display"> Display</a>
                              </li>
                          </ul>
                      </div>
                  </li>
              </ul>
          </li>
          <!-- <li class="li-hover">
            <div class="row">
              <div class="col s12 m12 l12">
                <div class="sample-chart-wrapper">
                  <div class="ct-chart ct-golden-section" id="ct2-chart"></div>
                </div>
              </div>
            </div>
          </li> -->
        </ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
      </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->

      <!-- START CONTENT -->
      <section id="content">
       <?=$content;?>
     </section>
     <!-- END CONTENT -->

     <!-- //////////////////////////////////////////////////////////////////////////// -->
     <!-- START RIGHT SIDEBAR NAV-->
     <aside id="right-sidebar-nav">
      <ul id="chat-out" class="side-nav rightside-navigation">
        <li class="li-hover">
          <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
          <div id="right-search" class="row">
            <form class="col s12">
              <div class="input-field">
                <i class="mdi-action-search prefix"></i>
                <input id="icon_prefix" type="text" class="validate">
                <label for="icon_prefix">Search</label>
              </div>
            </form>
          </div>
        </li>
        <li class="li-hover">
          <ul class="chat-collapsible" data-collapsible="expandable">
            <li>
              <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
              <div class="collapsible-body recent-activity">
                <div class="recent-activity-list chat-out-list row">
                  <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                  </div>
                  <div class="col s9 recent-activity-list-text">
                    <a href="#">just now</a>
                    <p>Jim Doe Purchased new equipments for zonal office.</p>
                  </div>
                </div>
                <div class="recent-activity-list chat-out-list row">
                  <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                  </div>
                  <div class="col s9 recent-activity-list-text">
                    <a href="#">Yesterday</a>
                    <p>Your Next flight for USA will be on 15th August 2015.</p>
                  </div>
                </div>
                <div class="recent-activity-list chat-out-list row">
                  <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                  </div>
                  <div class="col s9 recent-activity-list-text">
                    <a href="#">5 Days Ago</a>
                    <p>Natalya Parker Send you a voice mail for next conference.</p>
                  </div>
                </div>
                <div class="recent-activity-list chat-out-list row">
                  <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                  </div>
                  <div class="col s9 recent-activity-list-text">
                    <a href="#">Last Week</a>
                    <p>Jessy Jay open a new store at S.G Road.</p>
                  </div>
                </div>
                <div class="recent-activity-list chat-out-list row">
                  <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                  </div>
                  <div class="col s9 recent-activity-list-text">
                    <a href="#">5 Days Ago</a>
                    <p>Natalya Parker Send you a voice mail for next conference.</p>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
              <div class="collapsible-body sales-repoart">
                <div class="sales-repoart-list  chat-out-list row">
                  <div class="col s8">Target Salse</div>
                  <div class="col s4"><span id="sales-line-1"></span>
                  </div>
                </div>
                <div class="sales-repoart-list chat-out-list row">
                  <div class="col s8">Payment Due</div>
                  <div class="col s4"><span id="sales-bar-1"></span>
                  </div>
                </div>
                <div class="sales-repoart-list chat-out-list row">
                  <div class="col s8">Total Delivery</div>
                  <div class="col s4"><span id="sales-line-2"></span>
                  </div>
                </div>
                <div class="sales-repoart-list chat-out-list row">
                  <div class="col s8">Total Progress</div>
                  <div class="col s4"><span id="sales-bar-2"></span>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
              <div class="collapsible-body favorite-associates">
                <div class="favorite-associate-list chat-out-list row">
                  <div class="col s4"><img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                  </div>
                  <div class="col s8">
                    <p>Eileen Sideways</p>
                    <p class="place">Los Angeles, CA</p>
                  </div>
                </div>
                <div class="favorite-associate-list chat-out-list row">
                  <div class="col s4"><img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                  </div>
                  <div class="col s8">
                    <p>Zaham Sindil</p>
                    <p class="place">San Francisco, CA</p>
                  </div>
                </div>
                <div class="favorite-associate-list chat-out-list row">
                  <div class="col s4"><img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                  </div>
                  <div class="col s8">
                    <p>Renov Leongal</p>
                    <p class="place">Cebu City, Philippines</p>
                  </div>
                </div>
                <div class="favorite-associate-list chat-out-list row">
                  <div class="col s4"><img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                  </div>
                  <div class="col s8">
                    <p>Weno Carasbong</p>
                    <p>Tokyo, Japan</p>
                  </div>
                </div>
                <div class="favorite-associate-list chat-out-list row">
                  <div class="col s4"><img src="<?php echo PATH_IMG ?>avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                  </div>
                  <div class="col s8">
                    <p>Nusja Nawancali</p>
                    <p class="place">Bangkok, Thailand</p>
                  </div>
                </div>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </aside>
    <!-- LEFT RIGHT SIDEBAR NAV-->

  </div>
  <!-- END WRAPPER -->

</div>
<!-- END MAIN -->


<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START FOOTER -->
<!-- <footer class="page-footer">
  <div class="footer-copyright">
    <div class="container">
      <span>Copyright © 2015 <a class="grey-text text-lighten-4" href="http://themeforest.net/user/geekslabs/portfolio?ref=geekslabs" target="_blank">GeeksLabs</a> All rights reserved.</span>
      <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
    </div>
  </div>
</footer> -->
<!-- END FOOTER -->

    <!-- jQuery Library -->
    <!-- Possible fix for charts.js -->
    <!-- <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>jquery-2.2.4.min.js"></script> -->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>materialize.min.js"></script>
    <!--prism -->
    <script type="text/javascript" src="js/prism/prism.js"></script>

    <!--scrollbar-->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <!-- chartist -->
    <script type="text/javascript" src="<?php echo PATH_JS_PLUGIN ?>chartist-js/chartist.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="<?php echo PATH_JS ?>plugins.min.js"></script>

    <!-- AngularJS -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-filter/0.4.7/angular-filter.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
  	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
  	<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>
  	<script src="//ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.5.10/angular-sanitize.js"></script>

    <script type="text/javascript" src="<?php echo PATH_JS ?>angular/app.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS ?>angular/controllers/transactionsCtrl.js"></script>
    <!-- <script type="text/javascript" src="<?php echo PATH_JS ?>angular/directives/daterangepicker.js"></script> -->

  <?php
    $url = $this->uri->segment(1);

    switch($url)
    {
      case '':
        echo "<script src='" . PATH_JS . "amcharts/amcharts.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/serial.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/themes/light.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/themes/dark.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/themes/chalk.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/plugins/export/export.min.js'></script>";
        echo "<script src='" . PATH_JS . "charts.js'></script>";
        break;
        echo "<script src='" . PATH_JS . "amcharts/themes/light.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/themes/dark.js'></script>";
        echo "<script src='" . PATH_JS . "amcharts/themes/chalk.js'></script>";
        echo "<script src='" . PATH_JS . "charts.js'></script>";
      break;
      case 'transactions':
        echo "<script src='//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js'></script>";
        // echo "<script src='" . PATH_JS . "transactions.js'></script>";
        echo '<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/lodash.js/0.10.0/lodash.min.js"></script>';
        if ( $this->uri->segment(2) != 'view_transactions' )
          echo '<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>';
        echo '<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>';

        if ( $this->uri->segment(2) == 'view_transactions' ) {
          echo '<script type="text/javascript" src="'. PATH_JS_PLUGIN .'data-tables/js/jquery.dataTables.min.js"></script>';
        }
      break;

      case 'display':
      case 'users':
        echo '<script type="text/javascript" src="' . PATH_JS_PLUGIN . 'data-tables/js/jquery.dataTables.min.js"></script>';
        // echo '<script type="text/javascript" src="' . PATH_JS . '/plugins/data-tables/data-tables-script.js"></script>';
      break;
      default:
      break;
    }
  ?>

    <script type="text/javascript" src="<?php echo PATH_JS ?>custom-script.js"></script>

  </body>

</html>
